import Vue from 'vue'
import Hello from './vue-components/hello.vue'
import createMenu from './components/menu.js'

let data = ['Головна', 'Про мене', 'Портфоліо', 'Спорт', 'bo'],
    menu = createMenu(data, 'menu');

document.body.appendChild(menu);

new Vue({
    el: '#app',
    components: {
        Hello
    }
});