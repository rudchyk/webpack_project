const gulp = require('gulp'),
    webpack = require('webpack-stream'),
    webpackConfig = require('./webpack.config.js');

gulp.task('html', () => {
  return gulp.src('./src/tpl/index.html')
    .pipe(gulp.dest('dist/'));
});

gulp.task('js', () => {
  return gulp.src('./src/js/main.js')
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest('dist/js/'));
});

gulp.task('build', ['html', 'js']);

gulp.task('default', ['build']);