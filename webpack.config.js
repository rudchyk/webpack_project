const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

let sassResourcesLoader = {
    loader: 'sass-resources-loader',
    options: {
        resources: [
            './src/sass/sources/_variables.scss',
            './src/sass/sources/_mixins.sass'
        ]
    }
}

module.exports = {
    entry: './src/js/main.js',
    output: {
        filename: 'app.js'
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: true,
                    loaders: {
                        // <style lang="scss">
                        scss: ExtractTextPlugin.extract({
                            use: [
                                'css-loader',
                                'sass-loader',
                                sassResourcesLoader
                            ],
                            fallback: 'vue-style-loader'
                        }),
                        // <style lang="sass">
                        sass: ExtractTextPlugin.extract({
                            use: [
                                'css-loader',
                                'sass-loader?indentedSyntax',
                                sassResourcesLoader
                            ],
                            fallback: 'vue-style-loader'
                        }),
                        // <style>
                        css: ExtractTextPlugin.extract({
                            use: [
                                'css-loader'
                            ],
                            fallback: 'vue-style-loader'
                        })
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('components.css')
    ]
}





